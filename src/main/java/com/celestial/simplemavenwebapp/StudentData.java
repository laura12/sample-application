package com.celestial.simplemavenwebapp;
import java.sql.*;

/**
 *
 * @author Nic Nei
 */
public class StudentData {
	public String getName(int id) {
		String name = "unknown";
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			Connection connection = DriverManager.getConnection("jdbc:mysql://34.254.253.169:3306/qadb","root","ppp");
			Statement statement = connection.createStatement();
			ResultSet result = statement.executeQuery("SELECT name FROM students WHERE id="+id);
			if (result.isBeforeFirst()) {
				result.next();
				name = result.getString(1);
			}
			connection.close();
		}
		catch (Exception ex) {
			name = "Exception raised: "+ex.getMessage();
			ex.printStackTrace();
		}
		return name;
	}
	public static void main(String[] args) {
		StudentData studentData = new StudentData();
		String target = studentData.getName(1001);
		System.out.println(target);
	}
}
